+++
categories = [ "Tech" ]
date = "2021-04-28 17:17:27 +0930"
tags = [ "share", "links" ]
title = "I don't have a Linktree"
draft = true
+++

Over on [Rubenerd](https://rubenerd.com/i-have-a-linktree/) Ruben mentioned his [Linktr.ee account](https://linktr.ee/rubenerd). He thinks it's pointless because he has an about page and I'd say that his readers would get a fair sense of his interests just from his prolific writing, but I learned a few things and found some new sites this way, so I think it's an over-all good.

It reminded me of another service a fellow Mastodon user set up around the time Zoom bought Keybase. One of the things Keybase did well was help make secure key management a little bit easier, and encouraged you to prove you were you on a whole bunch of other websites. By putting a bit of cryptographic text on your other profiles around the web, you could effectively say that "the person you're talking to right now on Keybase is also the same person who posted on this Twitter account or that website". You or I don't need to worry too much about that sort of level of trust, but

