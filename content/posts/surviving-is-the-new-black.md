---
title: "Surviving Is the New Black"
date: 2021-02-24T10:55:07Z
draft: true
---

I want to take a break from our regularly[^polite-chuckle] scheduled[^polite-snort] content[^polite-guffaw] and talk about something I haven't talked about here for a while.

I started this site originally on a night when I couldn't sleep because my partner couldn't sleep. Huh - I just read the post again and it wasn't so much that she couldn't sleep as woke me up in a panic in the middle of the night because she couldn't move her body. She's had a chronic illness for about 18 years, and while this was a particularly scary episode right at the start when we were learning her limits[^spoons], we've learned to adapt. 

So it surprised me the other day, when I was particularly stressed about work, to find the post [_Are You OK?_](https://dougbelshaw.com/blog/2020/12/01/are-you-ok/) with the below chart shared by [Doug Belshaw](https://fosstodon.org/@dajbelshaw).

{{< figure src="//turbo.geekorium.au/images/StressContinuum.png" alt="A Chart of four columns from left to right. Thriving: I got this; Surviving: Something ain't right; Struggling: I can't keep this up; In Crisis: I can't survive this" title="Stress Continuum" attr="Colorado Covid Healthcare Worker Resources" attrlink="https://cohcwcovidsupport.org/" >}}

It surprised me because I genuinely thought I *was* OK. I mean, I knew I was finding work difficult, and while the ongoing pandemic wasn't particularly impacting me at the time, reading through the Surviving column was like reading a description of my entire life. And not just because of work, or the absurd and tragic events of 2020, but the trajectory of massive chunks of my life since we realised my partner would never be fully healthy again.

Thinking about it now, maybe working from home full time did take more of a mental toll than I expected, but at the time I read this chart, I felt I finally had a glimmer of why I feel like I've just stepped back from my life. 

Before 2020 I joined the local SES. SES is a community-staffed emergency assistance organisation in South Australia. I saw the massive good that the CFS (community fire service) did in the 2019 bushfires, and wanted to do something to help my local community too, but I also don't like being on fire so I joined the SES. I got through basic training, got a nice orange suit, and then covid hit and meant we all had to stay home from training for a few weeks. Training resumed again within two months, but I stayed away longer.

It wasn't until months later when I finally went back that I realised I have become this withdrawn smaller version of myself. Training that night was scary and left me feeling foolish and I found it more and more difficult to go. Not because of the virus, but because in the space of a couple of weeks I felt like I missed some milestone the others were present for. I'd forgotten how to tie the simple knots I'd picked up really easily, found starting conversations awkward, and felt the team had moved on without me. There's a dozen things I could have done during the time away to have preemptively fixed it, or steps I could have taken to push through that feeling, but I couldn't. 

The chart lists some things under "Surviving" that really hit home. I'm more easily overwhelmed and irritated, almost every activity I used to enjoy feels like a burden, and I just cannot roll with the punches like I used to. I look back at the last 10 years of my life and see these gigantic fluctuations in how I perceive the world, my roll in it, and my own worth. 

Some of this is to do with my own health. I don't undertake physical activity, and my sleep is all shot to hell. I'm certain the armchair life-coaches among you will declare that if I went for a jog or lifted that this would all sort itself out, but even at my most active and fittest periods, my 

[^polite-chuckle]: LOL.
[^polite-snort]: that would be too much like work.
[^polite-guffaw]: Now let's not get greedy.
[^spoons]: Her spoons if you will.
