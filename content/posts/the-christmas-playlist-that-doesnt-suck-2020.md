---
categories:
- Share
date: "2020-12-24 09:00:00 +0930"
draft: false
publishdate: "2020-12-24 09:00:00 +0930"
tags:
- christmas
- playlist
- tradition
title: The Christmas Playlist That Doesn't Suck (2020)
series:
- christmas playlist
---

{{< figure src="https://turbo.geekorium.au/images/grogu-christmas.jpg" alt="A toy figure of the character Grogu from TV show The Mandalorian. It's a green alien looking into the distance, there's festive lights in the distance behind them" attr="Christmas Baby Yoda (Grogu) by Sergiy Galyonkin" attrlink="https://flic.kr/p/2khiC4f" >}}

I've scoured Soundcloud to find the best Christmas music I could find. It's chock full of Christmas favourites that are guaranteed to:

1.  Bring Christmas cheer
2.  Not suck

{{< soundcloud 1170828793 >}}

The whole [Christmas Playlist That Doesn't Suck (2020)](https://soundcloud.com/screenbeard/sets/christmas-playlist-2020) is up on Soundcloud right now, go have a listen!

Thank you to all the artists who have shared their Christmas songs on SoundCloud for the rest of us!
