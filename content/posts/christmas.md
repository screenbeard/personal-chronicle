---
categories:
- Life
date: "2007-12-24 23:41:20 +0930"
draft: false
publishdate: "2007-12-24 23:41:20 +0930"
slug: christmas
tags:
- christmas
- family
- friend
- holiday
- message
title: Christmas
url: /christmas/
---

{{< figure src="https://turbo.geekorium.au/images/hats-off-to-christmas.jpg" attr="`Hats off to Christmas` by Georgie Sharp" attrlink="http://www.flickr.com/photos/georgiesharp/311749963/" >}}

Merry Christmas everyone.
