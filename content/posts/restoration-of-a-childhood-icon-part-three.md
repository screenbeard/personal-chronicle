---
categories:
- Tech
date: 2021-03-22 18:00:00 +1030
tags:
- part 3
- commodore 64
- retro
title: Restoration of a Childhood Icon Part Three
---

### Stalled

In parts [one]({{< ref "restoration-of-a-childhood-icon" >}}) and [two]({{< ref "restoration-of-a-childhood-icon" >}}) of this small series I tried to repair my childhood Commodore 64.

I've tried a lot since my last post and completely botched it, so all-in-all I'm feeling pretty confident in myself and happy with my progress.

### Testing, testing

{{< figure src="//turbo.geekorium.au/images/C%3D64/test_failure.jpg" alt="A screen with blocks in place of the correct characters" title="Failure to launch" attr="" attrlink="" >}}

Step one was to try the test cartridge when it arrived. Plugging it in I got a completely different unique garbage pattern, and a green border instead of the usual blue. This said to me that parts of the test cartridge were working, but that something NOT tested by the cart was to blame for the garbled pattern. A bit of research suggested the logic chip can sometimes output this sort of nonsense, although I was unable to find any pictures showing either of the issues I've experienced. I ordered a new 74LS373 logic chip and a backup 8500 CPU just in case, and sat quietly in the corner of the room for them to arrive.

### I'm not equipped for this

The new chips arrived - and as the CPU I have is socketed - it was a quick job to see if the new 8500 would help. It did not.
The logic chip was *not* socketed, and so I made a trip to Jaycar for one so I could start the process of making the whole board more easy to use for testing chips.

I did _not_ spend the money on a desoldering gun, and soon regretted it. I was not enjoying it, but I was at least making progress until the final pin of the old logic chip didn't de-solder the whole way and pulled up the trace on the board all the way back to the previous thru-hole.

I tracked back to Jaycar and purchased some silver trace paint and some wire. The trace paint was a waste of time (although I could have been doing it wrong), so I'm glad I picked up some wire while I was there. Then I installed the socket and tried to hard-wire the pin on the broken trace back to where it needed to be.

### One, Two, Three

{{< figure src="//turbo.geekorium.au/images/C%3D64/dodgy_soldering.jpg" alt="A close up of a circuit board with bad wiring and soldering visible" title="Ugh" attr="" attrlink="" >}}

My attempts had made things worse. With the new logic chip in place in the socket and my gross wiring, the display now cuts in and out. Mostly out. The crazy patterns are still there too, making it clear the logic chip was not the culprit so I also shoddily removed a potentially working logic chip. Finally, I thought it might have simply been a dodgy soldering job, so I rechecked my work, and in the process learned my soldering iron might be too hot for this sort of work, and I've melted parts of the socket and I'm worried I've damaged the PCB even further.

### Time to give up

At this point I was ready to pack it in. Deep down I'm not ready to stop, but I reached the limits of my knowledge about three steps ago, and have little to show for it. I reached out to friends who gave me some further advice, but I thought I'd sit on if for a while and try to decide the next best steps.

Fortunately I may have some better news for the next post in this series.
