---
categories:
- Rambling
date: "2018-04-02 03:04:43 +0930"
draft: false
publishdate: "2018-04-02 03:04:43 +0930"
slug: dashing-dynamic-duo
tags: []
title: Dashing Dynamic Duo
url: /dashing-dynamic-duo/
---

{{< figure src="https://turbo.geekorium.au/wp-content/uploads/2011/04/batmanandrobin-comicbook-logo-1.jpg" alt="The words 'Batman and Robin' inside a stylised bat for the comic book" title="...Running" >}}


I once did a post about the old-old [Batman and Robin Serial](//the.geekorium.au/batman-as-youve-never-seen-him/) and took some screen-caps from the episodes. One of them has proven quite popular around the internet and has been used by sites like ~~[Cracked]~~ [THE JOHNNY DUNCAN IMDB PAGE](https://m.imdb.com/name/nm0242001/) for any post that needs a picture of Batman and Robin running:

{{< figure src="//turbo.geekorium.au/wp-content/uploads/526941344_4c307467e1_o.jpg" alt="A grainy black and white still frame of Batman and Robin running towards the camera from the 1949 Batman and Robin serial">}}

I wondered why it was so popular, till I realised just how iconic that image is, even if I can't think of a particular example. So after some research, here's a compilation of images of Batman and Robin running from different media.

First up is Adam West and Burt Ward running to the UN to save some diplomats from being powdered by their greatest foes. I think they'd gotten stuck in traffic?

{{< figure src="https://turbo.geekorium.au/wp-content/uploads/batman-1960-movie.jpg" alt="Robin and Batman running towards the camera in a still from the 1966 Batman TV show" title="Kapow!" >}}

I can't find any images from the original comics, but I'll keep my eye out for them and put them up as they come. The closest I could find was this vintage 1966 Japanese comic cover. This is the Adam West era as you can tell by the eyebrows on the cowl[^shops].

{{< figure src="https://turbo.geekorium.au/wp-content/uploads/japanese-picture-book.jpg" alt="A Japanese picture book whose title is in Japanese and Batman and Robin are running toward three villains who are an anthropomorphic fox, black-skinned crocodile creature, and an eagle." title="What on earth is that weird black crocodile/mole creature?" >}}

Despite the lack of direct comic book proof, I submit this retro t-shirt design, and you tell me if it doesn't trigger a memory of something in your brain. You **know** this is a classic image. If you've come to realise the iconic-ness of the image of the Dynamic Duo running, you can buy [~~this t-shirt~~](//www.truffleshuffle.co.uk/store/mens-running-batman-robin-tshirt-from-junk-food-p-2859.html) and wear it proudly.

{{< figure src="https://turbo.geekorium.au/wp-content/uploads/Mens_Running_Batman_and_Robin_T_Shirt_from_Junk_Food_print_500_314_380_76.jpg" alt="Another image of Batman and Robin Running, on a T-Shirt" title="Mil... Can I have some cash?" >}}

Or if that doesn't convince you that Batman and Robin hoofing it is one of pop-culture's most classic images, try this piece from the 2007 Singapore Design Festival. Once glance and you immediately *get* it don't you? Because it's *classic*.

{{< figure src="https://turbo.geekorium.au/wp-content/uploads/To_the_rescue_by_Draken413o.jpg" alt="A parking zone sign with Batman and Robin silhouettes" attr="`to the rescue` by Draken413o" attrlink="//draken413o.deviantart.com/art/To-the-rescue-121441303" >}}

Here they are running in Batman: The Animated Series.

{{< figure src="https://turbo.geekorium.au/wp-content/uploads/batman-animated.jpg" alt="A cartoon Batman and Robin running towards the viewer from a still from Batman Animated Adventures" title="Animated!" >}}

Followed by an awesome wallpaper by BobbyRubio on Deviantart.

{{< figure src="https://turbo.geekorium.au/wp-content/uploads/midnight_run_by_barrypresh-d2xx189.jpg" alt="A drawn picture of Batman and Robin running across a rooftop" attr="`Midnight Run` by BobbyRubio" attrlink="//bobbyrubio.deviantart.com/art/Midnight-Run-177900921" >}}

The next is Batman and Nightwing, but as Nightwing started out as Robin[^dontstart], I figured it was an homage to the idea and therefore valid.

{{< figure src="https://turbo.geekorium.au/wp-content/uploads/batman-hush.jpg" link="//www.amazon.com/Batman-615-Hush-Part-Dead/dp/B000VZUDME" alt="Cover for Batman: Hush with Batman and Nightwing running towards the reader" title="Batman Hush" >}}

Which leads nicely into this one, which is the New Batman and Robin, who are Dick Grayson (the original Robin nee Nightwing) and Damian Wayne, Bruce Wayne's son[^seriously]. So the image comes full circle to the next generation.

{{< figure src="https://turbo.geekorium.au/wp-content/uploads/batman-robin-19.jpg" link="//1979semifinalist.wordpress.com/2010/10/21/january-2011-dc-cover-solicits-in-three-sentences-or-less/" alt="Cover for Batman and Robin 19 with Robin and Batman running towards the reader" title="Batman The Next Generation" >}}

And for gamers, Batman and Robin in the best Batman video game adaptation of all time.

{{< figure src="https://turbo.geekorium.au/wp-content/uploads/download.jpg" alt="A LEGO version of Batman and Robin running towards the camera on the cover art of the LEGO Batman game" title="Arkham Who?" >}}

Finally, I leave you with Batman and Robin from the end of Batman Forever movie from 1995.

{{< figure src="https://turbo.geekorium.au/wp-content/uploads/Batman-Forever-Ending-Scene-www.keepvid.com2_.gif" link="//youtu.be/jPE4CVEPMiA?t=1m24s" alt="An animated gif of Batman and Robin running out of smoke from the final scene of Batman Forever" title="This movie is now only remembered as not being as bad as the next one." >}}

[^withcheese]:With added cheese
[^shops]:And by having seen quite a few 'shops in my time
[^dontstart]:Don't even get me started
[^seriously]:I said, don't get me started...
