---
categories:
- Share
date: "2022-12-24 09:00:00 +0930"
draft: false
publishdate: "2022-12-24 09:00:00 +0930"
tags:
- christmas
- playlist
- tradition
title: The Christmas Playlist That Doesn't Suck (2022)
series:
- christmas playlist
description: A soundcloud playlist of Christmas music that doesn't suck, along with a post discussing my choices.
---

{{< figure src="https://turbo.geekorium.au/images/christmas-cookie-2022.jpg" alt="A decorated Rudolf the Reindeer biscuit with brown chocolate icing, a red m&m nose, two little white icing eyes and parts of pretzles for antlers" attr="by Evany" >}}

I've scoured Soundcloud to find the best Christmas music I could find. It's chock full of Christmas favourites that are guaranteed to:

1.  Bring Christmas cheer
2.  Not suck

This year I thought I'd write up my song choices and explain some of them. In particular, number 21 is special. If you're interested, you can [skip to the writeup after you hit play](#my-2022-playlist-writeup).

{{< soundcloud 1538940298 >}}

## My 2022 playlist writeup

1. First up is a wonderful choral/orchestral version of _O Come All Ye Faithful_. I've included a Dan Forrest arrangement of this song in 2016. It reminds me a little of Murray Gold's arrangement of the Doctor Who theme for Tennant, has this fast frantic chase in it. It's good.
2. I've never included the _Hallelujah Chorus_ in a Christmas playlist for some reason. It's never been "Christmassy" - but it is right? As much as any of these carols deserve to be here? This Kazoo version by the CSUF Kazoo Ensemble made Mil laugh. 
3. I like to include new songs or songs that aren't the usual staples every year. This year I have _This Is Not A Christmas Song_ by NEFFEX. It's punky. I like it. 
4. _Deck the Halls_ by Cimorelli is another choral carol, in a neat arrangement.
5. This 8-bit version of _Little Drummer Boy_ by Francisco Ramírez kicks off with the original Donkey Kong beat from my childhood, and if I'm not including an 8-bit track for myself regujarly, why am I even doing this?
6. I've included _I Want a Hippopotamus For Christmas_ in some version almost ever year since I first heard it. Would you believe I never heard this song until I made the 2013 playlist? It has the same energy as _I Saw Mummy Kissing Santa Clause_, but I like it better, and this version by laughingstock embraces the energy.
7. There are **so many** good versions of _Carol Of The Bells_ and I loved it as a kid when I heard it in _Home Alone_, so there's a version almost every year. This version by Candice Boyd is brilliant.
8. We joke every year that _Away In A Manger_ was the song my eldest would tear up at - which isn't funny in itself - but that she would actively avoid listening to this song because it did. So I've made it my mission to find the sweetest saddest version I can every year, and this version by Phil Wickham fits the bill. Love you A!
9. _Winter Wonderland_ is almost always filler. In Australia it doesn't snow and my family hates carols that imply it's not Christmas without a blizzard. **However** this Wes Reeve verson ISN'T the version you know and it's quite sweet and is completely different song!
10. This is a funky acapella version of _Angels We Have Heard On High_ by Just 6. And as I'm listening to it again writing this up, I feel like this is the hidden gem of this playlist hidden back here at number 10. It's worth the anticipation though.
11. I've had some truly great versions of _Star of Wonder_ on past playlists and this instrumental one by Craig Hamilton based on Sufjan Steven's version which I had on my 2013 playlist. 
12. _Silent Night_ is a staple, and this acapella arrangement by AUGUST 08 is really good.
13. This loungy version of _Joy To The World_ by Father Fortuna Music is groovy.
14. I try to include a little funk or electroswing or both in every playlist. Wolfgang Lohr puts out something that fits the bill every year, so they're on these lists a lot. This is a very funky version of _Frosty the Snowman_ with Odd Chap (another of my favourite artists) and Maskarade.
15. _O Holy Night_ is my favourite carol hands down. I think it's almost singlehandedly responsible for reminding me years ago that despite losing my faith I didn't have to give up on the music and joy of Christmas. Getting to sing along to this once a year makes me happy, and this version by Flower Face is lovely.
16. This is another one of those songs that we've imported and makes no sense in Aus. But it's a fun song, and it's literally called _The Christmas Song_ so it shows up in searches a lot. This bluesy, blousy version by SHEE is catchy.
17. Another one that made Mil laugh. This Reliant K version of _Good King Wenceslas_ is dumb and funny.
18. I'm a sucker for a duet, and I've included a couple over the years. Jason and Courtney Tompkins version of _Have Yourself A Merry Little Christmas_ is sweet and well sung.
19. Not immune to pop-culture's influence, and the fact my kids love the Guardians of the Galaxy, I've included _I Don't Know What Christmas Is (But Christmastime Is Here)_ by the Old 97's from this year's GotG Christmas Special.
20. I've included _Dance of the Sugar Gum Fairy_ every year, and **this** makes less sence as a Christmas song than the Hallelujah Chorus, but so many artists do it, it must be Christmassy somehow. But I like it, and this version by B&B PROJECT is unique and I love it.
21. OK, this track is kinda special, in that it was on the first playlist I made in 2012, and it's the ten year anniversary of my Christmas playlists. It's a swwet version of _We Wish You A Merry Christmas_ by a really young girl named Olivia. This year I was looking through my past favourites looking to see if anyone had done other songs I could use and found number 22 linked from this account.
22. This is Olivia again years later at 12 singing _Ave Maria_ and it's phenominal. Such a mindblowing change. What a great way to wrap out ten years of Christmas playlists!

This is also the first year I'm not including Jingle Bells! 10 years seems like as good a time as any to skip it, but I've included it in every other playlist. Go take a look at them too!

The whole [Christmas Playlist That Doesn't Suck (2022)](https://soundcloud.com/screenbeard/sets/christmas-playlist-2022) is up on Soundcloud right now, go have a listen!

Thank you to all the artists who have shared their Christmas songs on SoundCloud for the rest of us!
