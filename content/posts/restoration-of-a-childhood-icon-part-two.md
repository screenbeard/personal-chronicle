---
categories:
- Tech
date: 2020-11-06 15:00:22 +1030
draft: false
tags:
- part 2
- commodore 64
- retro
title: Restoration of a Childhood Icon Part Two
---

### 40-Year-Old Computers Are A Nightmare

Restoring my childhood computer is apparently going to take longer than I hoped. [Restoration of a Childhood Icon Part One]({{< ref "restoration-of-a-childhood-icon" >}}) ended on a bit of a bummer, and (spoiler warning) this part is also not the conclusion I'm looking for.

On [Mike, First of His Name](https://social.chinwag.org/@mike)'s advice, I ended up purchasing a set of new capacitors and a modern PLA replacement chip from [Retroleum.co.uk](https://www.retroleum.co.uk/commodore-64), then waited patiently for them to arrive.

### Like Riding A Bike

In preparation, I'd asked for a soldering iron for my birthday, and also grabbed myself some supplies so I could dredge my rusty soldering skills out of their 30 year hiatus.

With solder in hand, and solder sucker in... also... hand, I started by recapping the board. Fortunately, once you learn to solder it's not difficult enough to forget, so I soon had new capacitors in place.

However on testing, the old boy still wouldn't give me anything better than a black screen.

### Light At The End

{{< figure src="//turbo.geekorium.au/images/C%3D64/PLA.jpg" alt="A modern PLA chip replacement" title="Wanna PLA?" attr="" attrlink="" >}}

But fear not! I had the replacement PLA chip! And with this baby in place, I was...

Only slightly better off.

On testing, I'm getting somewhere with this new PLA, but it's clear that the PLA was **one** of the issues with the old boy, but not the only one. Now instead of a black screen, I get the familiar blue border indicating not everything is borked, but I get a jumble of colours instead of the expected BASIC intro text.

{{< figure src="//turbo.geekorium.au/images/C%3D64/garbled-display.jpg" alt="A CRT TV with a garbled Commodore 64 display" title="So near..." attr="" attrlink="" >}}

At this point Mike offered to take the board and run some tests and repair it himself, but I've come so far and want to see the repairs through myself if at all possible. So on his recommendation, I've ordered myself this [diagnostic and dead test cartridge](http://melbourneconsolerepros.com/product_info.php?cPath=22_32&products_id=137) to see if I can suss which chips are still not quite right.

[The Pictorial C64 Fault Guide](https://derbian.webs.com/c64diag/) doesn't show any issues quite like mine, so I need to do some digging of my own.

So stay tuned for part three, when I wire up my test harness and try and pry some chips.
