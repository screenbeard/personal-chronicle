---
categories:
- Tech
date: "2010-04-23 11:47:25 +0930"
draft: false
publishdate: "2010-04-23 11:47:25 +0930"
slug: tech-for-newbies
tags:
- new
- Site News
- technology
title: Tech for Newbies!
url: /tech-for-newbies/
---
I'm gonna try to get a new series going here on the Geekorium where I answer some of the questions I get asked in my job. I get asked for advice every day, and it's often more interesting than the sorts of things I actually do to get paid. Ages ago I toyed with the idea of making a site where I would break down technical concepts for the less technically minded, but wasn't sure it would have an audience. The advice I get asked for though already **has** an audience - the people that asked in the first place. So when I get asked a question I think warrants some fleshing out I'll put it under the new category [Tech for Newbies]({{< ref "/categories/tech/" >}}).

{{< figure src="//farm5.static.flickr.com/4041/4417256456_9cf47ab682.jpg" alt="A baby with a white onesie that has a stick figure holding a red semaphore flag, with the word n00b written underneath" title="omg n00b!" attr="Kim Unertl" attrlink="//www.flickr.com/photos/kimtimnashville/4417256456/" >}}

Keep in mind if you're technically minded, that the people I give this advice to aren't. My answers are simplified and often lacking some of the stuff us geeks find **very** important. Feel free to point this stuff out in the comments, but try not to be too harsh on my for leaving it out! Also sometimes I don't actually **have** an answer - it doesn't mean I won't try to put them on the right track.

And if you're not a geek maybe I can help you out. Leave a suggestion (there's a suggestion tab just over there to the right) if you have a question you think I could answer. Make sure to read what the geeks have to add though!

The first topic I'm tackling is the age old dilemma - [Should I Buy a Mac?]({{< ref "should-i-buy-a-mac" >}})
