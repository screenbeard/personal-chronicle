---
categories:
- Invention
date: "2007-12-05 11:32:57 +0930"
draft: false
publishdate: "2007-12-05 11:32:57 +0930"
slug: hair-passes-forward-screams-pass-backward
tags:
- comedy
- create
- shaving
title: Hair passes forward, screams pass backward
url: /hair-passes-forward-screams-pass-backward/
---

{{< figure src="//farm3.staticflickr.com/2080/2088101939_8170cafeda_o.png" alt="" title="I hate that Saturday guy... but Sunday is just a victim like me" attr="" attrlink="" >}}

Inspired by [Wellington Grey](http://wellingtongrey.net).