---
categories:
- Life
date: "2008-05-12 05:14:54 +0930"
draft: false
publishdate: "2008-05-12 05:14:54 +0930"
slug: pain-and-agony-awareness-day
tags:
- cfs
- chronic fatigue
- Conditions and Diseases
- fibromyalgia
- fms
- Health
- me
- Musculoskeletal Disorders
- Neurological Disorders
title: Pain and Agony Awareness Day
url: /pain-and-agony-awareness-day/
---

{{< figure src="//farm3.static.flickr.com/2158/2226802594_f4bac55da4.jpg" alt="A painting of a woman facing away from the viewer, surrounded by blue sky, birds and flowers. Down her back are three lumps with red pain lines coming from it, and from her hip and neck" attr="`27/365: fractured reality/grace under pain` by Samantha Kira Harding" attrlink="http://www.flickr.com/photos/55242755@N00/2226802594/" >}}

Sorry 'bout the title. It's Chronic Fatigue and Fibromyalgia Awareness Day [again](//the.geekorium.au/bluehair). I haven't done anything this year to raise awareness, except I've tried to sum up [Chronic Fatigue](http://twitter.com/joshnunn/statuses/809079681) and [Fibromyalgia](http://twitter.com/joshnunn/statuses/809083353) in 140 characters on [Twitter](http://www.twitter.com "Twitter").

[Natalie](//the.geekorium.au/bluehair/#comment-122) left a comment on my post from two years ago, and told me about the videos she's been making on Youtube. I particularly thought this one appropriate:

{{< youtube EyAZ3SsyLGo >}}

P.S. Also - Happy Birthday mum.
