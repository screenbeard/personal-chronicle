---
slug: wishlist
title: Wish List
---

### The Mayhem In Monsterland collector's edition box set

![](Mayhem25th_sm.jpg)

This is the quintessential game from my childhood that I never got to play!

http://binaryzone.org/retrostore/index.php?main_page=product_info&products_id=373

Approx $70 (converted from GBP + shipping)


### Almost Anything from MathsGear

![](go_first_dice.webp)

* [Things to Make and Do in the Fourth Dimension](https://mathsgear.co.uk/collections/books/products/signed-paperback-things-to-make-and-do-in-the-fourth-dimension)
* [Go First Dice](https://mathsgear.co.uk/collections/dice/products/go-first-dice)
* [Dodecaplex Puzzle](https://mathsgear.co.uk/collections/all-products/products/dodecaplex-puzzle)
* [A D120 Die](https://mathsgear.co.uk/collections/dice/products/d120-dice)

### MagneticCube

![](scrambled.png)

* [A Wooden Dice Cube](https://magneticcube.com/home/olie-a4gsh9) 
* [A Black Multicolour Dice Cube](https://magneticcube.com/home/olie-86tkqx) OR

### A joystick switcher

* http://melbourneconsolerepros.com/product_info.php?products_id=136

