<?php

$file = '/var/tmp/output.md';

$ch = curl_init();
curl_setopt_array($ch, [
    CURLOPT_URL => getenv('MINIFLUX_URL') . '/feeds',
    CURLOPT_HTTPHEADER => [
        "X-Auth-Token: " . getenv('AUTH_TOKEN')
    ],
    CURLOPT_RETURNTRANSFER => true,
]);

$feeds = json_decode(curl_exec($ch), true);

$fp = fopen($file, 'w');
fwrite($fp, "---\nslug: " . getenv('SLUG') . "\ntitle: " . getenv('TITLE') . "\n---\n");
fwrite($fp, getenv('DESCRIPTION') . PHP_EOL . PHP_EOL);
$category = getenv('CATEGORY_FILTER') ?? null;
foreach ($feeds as $feed) {
    if ($category === null || $feed['category']['id'] == $category){
        $icon = '<img class="favicon" src="data:' . getenv('DEFAULT_ICON') . '" alt="no favicon">';
        if(getenv('ICONS') == true && $feed['icon'] !== null){
            curl_setopt_array($ch, [
                CURLOPT_URL => getenv('MINIFLUX_URL') . '/feeds/' . $feed['id'] . '/icon',
            ]);
            $icon_data = curl_exec($ch);
            if (!curl_error($ch)){
                $icon_data = json_decode($icon_data, true);
                $icon = '<img class="favicon" src="data:' . $icon_data['data'] . '" alt="' . $feed['title'] . ' - favicon">';
            }
        }
        fwrite($fp, '- ' . $icon . '[' . $feed['title'] . '](' . $feed['site_url'] . ') - [Feed](' . $feed['feed_url'] . ')'. PHP_EOL);
    }
}
fclose($fp);

